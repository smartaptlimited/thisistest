const express = require('express')
const path = require('path')
const stylus = require("stylus");
const db = require(__dirname + '/lib/db')
const pgp = require('pg-promise')({})
// const bulma = require('bulma')

const uuid = require('uuid/v4');
const bodyParser = require('body-parser')

const app = express()
const PORT = process.env.PORT || 5000

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')
app.use(stylus.middleware({
    src: path.join(__dirname, 'assets/stylus'),
    dest: path.join(__dirname, 'public/css')
}));
app.use(express.static(__dirname + '/public'))

app.listen(PORT, (err) => {
  console.log(`running server on port: ${PORT}`);
});

// pgp.as.format test.
// const query = "SELECT * FROM ??";
// const table = ["users"];
// const tests = pgp.as.format(query, table);
// console.log(tests);

app.get('/',(req,res)=>{

	db.any('select * from users')
	.then(data => {

		console.log(data)

    res.render('index', {
      title: "Dash board",
      id: data[0]["user_id"],
      name: data[0]["name"],
      data: data
    })
	})

});

app.get('/register',(req,res)=>{

  // console.log(req.url);

  res.render('register', {
    title: "Users add"
  })

});

app.post('/register', (req,res,next)=>{

  const user_id = uuid();
  const user_name = req.body.user_name;

  console.log(user_name);
  console.log(user_id);

  db.none('INSERT INTO users(user_id, name) VALUES($1, $2)', [user_id, user_name])
    .then(() => {
        res.redirect('/');
    })
    .catch(error => {
        // error;
    });

});

app.get('/user/:name', (req,res)=>{

  // const query = "SELECT * FROM ?? WHERE ??=?";
  // const table = ["users","name",req.params.name];
  // query = pgp.as.format(query, table);
  //
  // console.log(query);

	db.one('SELECT * FROM users WHERE name = $1', req.params.name)
    .then(user => {

        console.log(user);

        res.render('index', {
          title: "Dash board",
          id: user["user_id"],
          name: user["name"]
        })
      })
      .catch(error => {
        console.log("その値はDB上に存在しません。");
      });

});

app.get('/images',(req,res)=>{

  console.log("VISIT A PAGE => /images")

  res.render('images', {
    title: "View images"
  })

});

app.get('/images/add',(req,res)=>{

  console.log("VISIT A PAGE => /images_add")

  res.render('images_add', {
    title: "Add images"
  })

});

// 任意の写真を提供するAPI
// app.get("/api/photo/:photoId", function(req, res, next){
//     // req.params.photoIdでユーザーが指定した写真IDを識別できる
//     console.log(req.params.photoId);
//     next()
// });
