CREATE TABLE images (
  user_id uuid not null references users(user_id),
  image_id uuid not null primary key,
  image_url varchar not null,
  display_flg boolean not null default true,
  delete_flg boolean not null default false
)

DROP TABLE images;
